// Fill out your copyright notice in the Description page of Project Settings.


#include "ToonTankGameMode.h"
#include "Tank.h"
#include "Tower.h"
#include "ToonTanksPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"


void AToonTankGameMode::ActorDied(AActor *DeadActor)
{

    if (DeadActor == Tank)
	{
		
		Tank ->HandleDestruction();
		if (ToonTanksPlayerController)
		{
			ToonTanksPlayerController->SetPlayerStateEnable(false);
			
		}
		GameOver(false);
       
		
	}	

	else if (ATower *DestroyedTower = Cast<ATower>(DeadActor))
	{
		DestroyedTower->HandleDestruction();
		
		 --TargetTowers;

		 if (TargetTowers == 0)
		 {
			GameOver(true);
		 }
		 

	
	}
  
  
     
}

void AToonTankGameMode::BeginPlay()
{
    Super::BeginPlay();
    HandleGameStart();
 
}


void AToonTankGameMode::HandleGameStart()
{  
	TargetTowers = GetTargetTowersCount();

    Tank = Cast<ATank>(UGameplayStatics::GetPlayerPawn(this,0));
	ToonTanksPlayerController = Cast<AToonTanksPlayerController>(UGameplayStatics::GetPlayerController(this, 0));

     StartGame();


	if (ToonTanksPlayerController)
	{
        ToonTanksPlayerController->SetPlayerStateEnable(false);

       FTimerHandle TimerHandle;
	   FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(ToonTanksPlayerController, &AToonTanksPlayerController::SetPlayerStateEnable, true); 
       GetWorldTimerManager().SetTimer(TimerHandle, TimerDelegate, StartDelay, false ); 

	}

}

int32 AToonTankGameMode::GetTargetTowersCount()
{

   TArray<AActor*> Towers;
   UGameplayStatics::GetAllActorsOfClass(this,ATower::StaticClass(), Towers);
   return Towers.Num();

}
   
 
    

	





