// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "Tank.generated.h"


UCLASS()
class TOONTANKS_API ATank : public ABasePawn
{
	GENERATED_BODY()

    public:
	// Sets default values for this pawn's properties
	ATank();
 
     void HandleDestruction();

    
	APlayerController* GetTankPlayerController() const {return TankPlayerController;}


protected:

	// Called when the game starts or when spawnedB
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	bool bAlive = true;

private:	

       UPROPERTY(EditDefaultsOnly, Category = "MovementSpeed")
		   float Speed = 900.f;

       UPROPERTY(EditDefaultsOnly, Category = "MovementSpeed")
           float TurnRate = 50.f;

         UPROPERTY(EditDefaultsOnly)
	       class USpringArmComponent* SpringArm;

         UPROPERTY(EditDefaultsOnly)
   	       class UCameraComponent* Camera;


            void MoveForward(float Value);  
            void Turn(float Value);
            

	       class APlayerController* TankPlayerController;
     
	     



};
