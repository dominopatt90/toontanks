// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/DamageType.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Camera/CameraShakeBase.h"


// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
      
	  ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
      RootComponent = ProjectileMesh;

      ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
      ProjectileMovement ->InitialSpeed = 1300.f;
      ProjectileMovement ->MaxSpeed = 1300.f;
      
	  SmokeTrailParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Smoke Trail Particle"));
      SmokeTrailParticle ->SetupAttachment(RootComponent); 
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();

    ProjectileMesh->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);

	UGameplayStatics::PlaySoundAtLocation(this,LaunchSound,GetActorLocation());
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectile::OnHit(UPrimitiveComponent *HitComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, FVector NormalImpusle, const FHitResult& Hit)
{

     AActor* MyOwner = GetOwner();

	if (MyOwner == nullptr) 
	{
		Destroy();

		return;
	}
	 
	AController* MyOwnerInstigator = MyOwner->GetInstigatorController();
	UClass* DamageTypeClass = UDamageType::StaticClass();


	if (OtherActor && OtherActor != this && OtherActor != MyOwner)
	{
		UGameplayStatics::ApplyDamage(OtherActor, Damage, MyOwnerInstigator, this, DamageTypeClass);
		
		if (HitParticle)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this,HitParticle, GetActorLocation(), GetActorRotation());
			
			
		}
		if (HitSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this,HitSound,GetActorLocation());
		}
        if (HitCameraClass)
		{
			GetWorld() ->GetFirstPlayerController()->ClientStartCameraShake(HitCameraClass);
		}
		

	}   

         Destroy();
    

}

