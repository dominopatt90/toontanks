// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Tank.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Actor.h"
#include "TimerManager.h"


// Sets default values
ATower::ATower()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    


}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
    Tank = Cast<ATank>(UGameplayStatics::GetPlayerPawn(this,0)); 

    GetWorldTimerManager().SetTimer(FireRateTimerHamdle,this, &ATower::CheckFireCondition,FireRate,true);

}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	 
    if (InFireRange())
    {
       RotateTurret(Tank ->GetActorLocation());
    }
    
      
           
    
     
}

 void ATower::HandleDestruction()
 {

     Super::HandleDestruction();
     Destroy();


 }



void ATower::CheckFireCondition()
{
   if (Tank == nullptr)
   {
      return;
   }
   

   if (InFireRange() && Tank->bAlive)
   {
      Fire();
   }

}
bool ATower::InFireRange()
{
     if (Tank)
      {
         float Distance = FVector::Dist(GetActorLocation(), Tank ->GetActorLocation());

         if (Distance <= FireRange)
         {
            return true;  
         }   
      }
      return false;
}
