// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"




UCLASS()
class TOONTANKS_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


private:

 
       UPROPERTY(EditDefaultsOnly)
         float Damage = 50;
    

       UPROPERTY(EditDefaultsOnly)         
		 class UStaticMeshComponent* ProjectileMesh;

       UPROPERTY(EditDefaultsOnly)  
		 class UProjectileMovementComponent* ProjectileMovement;


	   UFUNCTION()
	     void OnHit(UPrimitiveComponent *HitComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, FVector NormalImpusle, const FHitResult& Hit);  


     UPROPERTY(EditAnywhere, Category = "Combat")  
      class UParticleSystem *HitParticle;
     UPROPERTY(VisibleAnywhere, Category = "Combat")  
      class UParticleSystemComponent *SmokeTrailParticle;

     UPROPERTY(EditAnywhere, Category = "Combat")
	  class USoundBase* LaunchSound;

     UPROPERTY(EditAnywhere, Category = "Combat")
	  class USoundBase* HitSound;

	  UPROPERTY(EditAnywhere, Category = "Combat")
      TSubclassOf<class UCameraShakeBase> HitCameraClass;

            
};
