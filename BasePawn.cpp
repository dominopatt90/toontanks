// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePawn.h"
#include "Projectile.h"
#include "Components/CapsuleComponent.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/World.h"
#include "Particles/ParticleSystem.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
ABasePawn::ABasePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComp")); 
	  RootComponent = CapsuleComp; 
		 
	  BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	  BaseMesh ->SetupAttachment(CapsuleComp);

	  TurretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TurretMesh"));
    TurretMesh ->SetupAttachment(BaseMesh);

	  ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("ProjectileSpawnPoint"));
    ProjectileSpawnPoint ->SetupAttachment(TurretMesh);
 
       

}

void ABasePawn::HandleDestruction()
{
    

     
     if (DeathParticle)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, DeathParticle, GetActorLocation(), GetActorRotation());
           

	}

     if (DeathSound)
    {
      UGameplayStatics::PlaySoundAtLocation(this,DeathSound,GetActorLocation());
    }
    if (DeathCameraShakeClass)
    {
        GetWorld()->GetFirstPlayerController()->ClientStartCameraShake(DeathCameraShakeClass);
    }
    
}


void ABasePawn::RotateTurret(FVector LookAtTarget)
{
    FVector ToTarget = LookAtTarget - TurretMesh ->GetComponentLocation();
    FRotator LookAtRotation = FRotator(0.f, ToTarget.Rotation().Yaw, 0.f);
    TurretMesh ->SetWorldRotation(LookAtRotation);

}
void ABasePawn::Fire()
{
	      FVector SpawnLocation = ProjectileSpawnPoint ->GetComponentLocation();
        FRotator SpawnRotation = ProjectileSpawnPoint ->GetComponentRotation();

        AProjectile* Projectile = GetWorld() ->SpawnActor<AProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
             Projectile ->SetOwner(this);
}
