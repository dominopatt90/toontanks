// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "Tower.generated.h"

/**
 * 
 */
UCLASS()
class TOONTANKS_API ATower : public ABasePawn
{
	GENERATED_BODY()

	 public:
	// Sets default values for this pawn's properties
	ATower();

    void HandleDestruction();


	 
protected:
	// Called when the game starts or when spawnedB
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


private:	

       class ATank* Tank;
       FTimerHandle FireRateTimerHamdle; 	


      UPROPERTY(EditDefaultsOnly, Category = "Combat")
	    float FireRange = 900.f;  

      UPROPERTY(EditDefaultsOnly, Category = "Combat")
        float FireRate = 2.f;
	 

       void CheckFireCondition();
	   bool InFireRange();
       
	
};

